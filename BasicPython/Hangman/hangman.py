"""
HANGMAN GAME

In this exercise, we develop an automatic player for the hangman game.
"""

# Consider the following set of periodic elements as words to guess in the hangman game:
hangman_words = {'hydrogen', 'helium', 'lithium', 'beryllium', 'boron', 'carbon', 'nitrogen', 'oxygen', 'fluorine', 
                'neon', 'sodium', 'magnesium', 'aluminum', 'silicon', 'phosphorus', 'sulfur', 'chlorine', 'argon', 
                'potassium', 'calcium', 'scandium', 'titanium', 'vanadium', 'chromium', 'manganese', 'iron', 'cobalt', 
                'nickel', 'copper', 'zinc', 'gallium', 'germanium', 'arsenic', 'selenium', 'bromine', 'krypton', 
                'rubidium', 'strontium', 'yttrium', 'zirconium', 'niobium', 'molybdenum', 'technetium', 'ruthenium', 
                'rhodium', 'palladium', 'silver', 'cadmium', 'indium', 'tin', 'antimony', 'tellurium', 'iodine', 'xenon', 
                'cesium', 'barium', 'lanthanum', 'cerium', 'praseodymium', 'neodymium', 'promethium', 'samarium', 'europium', 
                'gadolinium', 'terbium', 'dysprosium', 'holmium', 'erbium', 'thulium', 'ytterbium', 'lutetium', 'hafnium', 
                'tantalum', 'tungsten', 'rhenium', 'osmium', 'iridium', 'platinum', 'gold', 'mercury', 'thallium', 'lead', 
                'bismuth', 'polonium', 'astatine', 'radon', 'francium', 'radium', 'actinium', 'thorium', 'protactinium', 
                'uranium', 'neptunium', 'plutonium', 'americium', 'curium', 'berkelium', 'californium', 'einsteinium', 
                'fermium', 'mendelevium', 'nobelium', 'lawrencium', 'rutherfordium', 'dubnium', 'seaborgium', 'bohrium', 
                'hassium', 'meitnerium'}

# before moving forward, let's learn about 2 Python built-in functions:
"""
enumerate Python built-in function
https://docs.python.org/3/library/functions.html#enumerate
"""
assert list(enumerate('apple')) == [(0, 'a'), (1, 'p'), (2, 'p'), (3, 'l'), (4, 'e')]
assert list(enumerate(['apple', 'banana', 'grapes', 'pear'])) == [(0, 'apple'), (1, 'banana'), (2, 'grapes'), (3, 'pear')]

"""
filter Python built-in function
https://docs.python.org/3/library/functions.html#filter
"""
def even(i):
    return (i%2) == 0
assert list(filter(even, {0, 1, 3, 56, 57, 58})) == [0, 56, 58]

# It's also possible to use a small anonymous function ("lambda function"):
assert list(filter(lambda i: i%2 == 0, {0, 1, 3, 56, 57, 58})) == [0, 56, 58]

"""
THE GAME - HUMAN VERSION

We give you the main code of the hangman game.
It interacts with the user through the console.
Try it:
"""
import random
 
def input_user(pattern, guesses):
    while True:
        print('previous guesses: ', guesses)
        l = input('please guess a new letter: ')
        if l in list('abcdefghijklmnopqrstuvwxyz') and l not in guesses:
            return l
 
def hangman_game(secret_word, input_method):
 
    # initialize the pattern to '***..**'
    pattern = '*' * len(secret_word)
    # initialize the number of turns to 10
    turns = 10
    # empty guess set
    guesses = set()
 
    # main loop allowing to keep guessing when the secret word is not totally guessed yet 
    # and there are still guesses left
    while True:
 
        # interactions with the player
        print(pattern, 'still', turns, 'guesses left')
        letter_guessed = input_method(pattern, guesses) # the user or the AI algo
        guesses.add(letter_guessed)
 
        if letter_guessed in secret_word:          
            # update the pattern
            new_pattern = ''
            for index, letter in enumerate(secret_word):
                new_pattern += (letter if letter == letter_guessed else pattern[index])
            pattern = new_pattern
 
            print('Excellent: '+ pattern)
 
            # test whether the word is guessed
            if '*' not in pattern:
                print('Done: You win!')
                break
 
        else:
            print('Sorry,', letter_guessed ,'is not in the secret word')
            turns -= 1
            if turns == 0:
                print('Game over! it was:', secret_word)
                break
 
secret_word = random.choice(list(hangman_words))
hangman_game(secret_word, input_user) 
# When you're done trying out the program, comment out the previous line
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

"""
We want to replace the user with an automated algorithm, 
ie. replace the 'input_user' function by an 'input_AI' function.

One strategy is:
 * Collect the set of words following the current pattern from the base of words 
   we are guessing from;
 * Calculate the probability of each letter 
   (the number of occurrences of a letter with respect to the number of occurrences of all letters)
   that is not yet guessed based on the collected set of words;
 * Choose the letter with the highest probability. 
"""


"""
Question 1

To develop an automated algorithm playing Hangman, we want to refine our set of words 
by only keeping those compatible with the currently guessed word 
with exactly the same letter in the same position for each already correctly guessed letter.
At each step of the game, we will need to collect the subset of words of some pattern, 
for example **a***m. 

We provide a function collect_v1(words, pattern) 
to collect from the words set those following a given pattern. 
We also take into account the size of the pattern to only keep words of the right length.

In this code, we purposely use three loop features :
  * continue: skips to the next iteration of the nearest enclosing loop;
  * break: terminates the nearest enclosing loop;
  * for ... else: the else clause runs when no break occurs in the for loop 

Detailed examples can be found here: 
https://docs.python.org/3/tutorial/controlflow.html#break-and-continue-statements-and-else-clauses-on-loops
 

Read/understand and comment each line of code of the function collect_v1(words, pattern); 
""" 
def collect_v1(words, pattern):
    pattern_elts = [(index, letter) for (index, letter) in enumerate(pattern) if (letter != '*')] 
    if not pattern_elts : 
        return set(filter(lambda w: len(w) == len(pattern), words))
    collected_words = set() 
    for word in  words:
        if len(word) != len(pattern): 
            continue
        for index, letter in pattern_elts:
            if (word[index] != letter) :
                break
        else: 
            collected_words.add(word)  
    return collected_words
 
assert collect_v1(hangman_words, '*a**i*m') == {'gallium', 'hafnium', 'calcium', 'hassium', 'cadmium'}
assert collect_v1(hangman_words, '*a**a***') == {'tantalum'}
assert collect_v1(hangman_words, '*a**a****') == {'palladium', 'manganese'}
assert collect_v1(hangman_words, '*a**a**') == set()
assert collect_v1(hangman_words, '*a*ra**') == set()
assert collect_v1(hangman_words, '***') == {'tin'}
assert collect_v1(hangman_words, '**') == set()

"""
Question 2

The three loop primitives introduced above (continue, break, for...else) are very convenient
but they are only "syntactic sugar" (https://en.wikipedia.org/wiki/Syntactic_sugar) 
and can be handled otherwise. 
Write a version collect_v2(words, pattern) without using any of them. 
"""
def collect_v2(words, pattern):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#assert collect_v2(hangman_words, '*a**i*m') == collect_v1(hangman_words, '*a**i*m')
#assert collect_v2(hangman_words, '*a**a***') == collect_v1(hangman_words, '*a**a***')
#assert collect_v2(hangman_words, '*a**a****') == collect_v1(hangman_words, '*a**a****')
#assert collect_v2(hangman_words, '*a**a**') == collect_v1(hangman_words, '*a**a**')
#assert collect_v2(hangman_words, '*a*ra**') == collect_v1(hangman_words, '*a*ra**')
#assert collect_v2(hangman_words, '***') == collect_v1(hangman_words, '***')

"""
Optimized data structure
------------------------

Our objective is to implement a collect function that is much faster than the obvious version 
given above. 
To do so we need to use an ad-hoc data structure that is well-adapted to our problem.

Let us save the collection of words in a new data structure combining both 
Python dictionaries and Python sets. 

Our ad-hoc base of words is a dictionary where :
 * a key is a tuple of the form (i, 'l') and
 * a value is the set of words having the letter 'l' at position i

For example, for the three words ['ten', 'tea', 'that'] we obtain the following base: 
base = {(0, 't'): {'ten', 'tea', 'that'}, 
        (1, 'e'): {'ten', 'tea'}, 
        (2, 'n'): {'ten'}, 
        (2, 'a'): {'tea', 'that'}, 
        (1, 'h'): {'that'}, 
        (3, 't'): {'that'}}

We may notice here that we want a data structure capable to answer search queries very quickly, 
even if it is achieved at the cost of redundancy and higher usage of memory space.
"""

"""
Question 3

Write a function create_base(words) which takes a list of words as parameter and returns such a base.
"""
def create_base(words):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#hangman_base = create_base(hangman_words)
#assert len(hangman_base) == 154
#assert hangman_base[(2, 'd')] == {'cadmium', 'radon', 'radium', 'hydrogen', 'gadolinium', 'indium', 'sodium', 'iodine'}
#assert hangman_base[(12, 'm')] == {'rutherfordium'}

"""
Question 4

Write a function collect_v3(words, base, pattern) to collect from the set of words 
together with the ad-hoc data structure base the words following a given pattern.
""" 
def collect_v3(words, base, pattern):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#assert collect_v3(hangman_words, hangman_base, '*a**i*m') == collect_v1(hangman_words, '*a**i*m')
#assert collect_v3(hangman_words, hangman_base, '*a**a***') == collect_v1(hangman_words, '*a**a***')
#assert collect_v3(hangman_words, hangman_base, '*a**a****') == collect_v1(hangman_words, '*a**a****')
#assert collect_v3(hangman_words, hangman_base, '*a**a**') == collect_v1(hangman_words, '*a**a**')
#assert collect_v3(hangman_words, hangman_base, '*a*ra**') == collect_v1(hangman_words, '*a*ra**')
#assert collect_v3(hangman_words, hangman_base, '***') == collect_v1(hangman_words, '***')

"""
Execution time observation
--------------------------
The 'timeit' module of Python allows us to measure the execution time of a Python code 
that we may run many times using the parameter number. 

Try the following, what do you observe?
"""
from timeit import timeit
# Uncomment the next 2 lines: 
#print(timeit(lambda : collect_v1(hangman_words, '*a**a***'), number=10000))
#print(timeit(lambda : collect_v3(hangman_words, hangman_base, '*a**a***'), number=10000))

"""
Let us consider a bigger set of words. 
Download in your working directory, next to your code, the English dictionary dic.txt 
with more than 235000 words available on Edunao.

Make sure your terminal current directory is your working directory and
try the following; what do you observe? 
"""
# Uncomment the next 5 lines:
#dic_words = {word.replace("-", "").replace(" ", "") for word in open("dic.txt").read().lower().splitlines()}
#dic_base = create_base(dic_words)
#assert collect_v1(dic_words, '*a**a***') == collect_v3(dic_words, dic_base, '*a**a***')
#print(timeit(lambda : collect_v1(dic_words, '*a**a***'), number=100))
#print(timeit(lambda : collect_v3(dic_words, dic_base, '*a**a***'), number=100))


"""
Question 5 (advanced only)

Rather than filtering afterwards the collected words by their size 
to only keep those of the size of the pattern, 
propose a better data structure to implement the base of words 
and to improve the collect queries.

Compare time performances.

Hint: you can organize words in a Python dictionary depending on their size 
on the top of our ad-hoc data structure:

base = {3: {(0, 't'): {'ten', 'tea'}, 
            (1, 'e'): {'ten', 'tea'}, 
            (2, 'n'): {'ten'},
            (2, 'a'): {'tea'}}, 
        4: {(0, 't'): {'that'}, 
            (1, 'h'): {'that'}, 
            (2, 'a'): {'that'}, 
            (3, 't'): {'that'}}}
"""
def create_base_v2(words):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#assert create_base_v2(['ten', 'tea', 'that']) == {3: {(0, 't'): {'ten', 'tea'}, 
#                                                      (1, 'e'): {'ten', 'tea'}, 
#                                                      (2, 'n'): {'ten'},
#                                                      (2, 'a'): {'tea'}}, 
#                                                  4: {(0, 't'): {'that'}, 
#                                                      (1, 'h'): {'that'}, 
#                                                      (2, 'a'): {'that'}, 
#                                                      (3, 't'): {'that'}}}

def collect_v4(words, base, pattern):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE FOLLOWING TESTS
    raise NotImplementedError

#hangman_base_v2 = create_base_v2(hangman_words)

#assert collect_v4(hangman_words, hangman_base_v2, '*a**i*m') == collect_v1(hangman_words, '*a**i*m')
#assert collect_v4(hangman_words, hangman_base_v2, '*a**a***') == collect_v1(hangman_words, '*a**a***')
#assert collect_v4(hangman_words, hangman_base_v2, '*a**a****') == collect_v1(hangman_words, '*a**a****')
#assert collect_v4(hangman_words, hangman_base_v2, '*a**a**') == collect_v1(hangman_words, '*a**a**')
#assert collect_v4(hangman_words, hangman_base_v2, '*a*ra**') == collect_v1(hangman_words, '*a*ra**')
#assert collect_v4(hangman_words, hangman_base_v2, '***') == collect_v1(hangman_words, '***')

#print(timeit(lambda : collect_v1(hangman_words, '*a**a***'), number=10000))
#print(timeit(lambda : collect_v3(hangman_words, hangman_base, '*a**a***'), number=10000))
#print(timeit(lambda : collect_v4(hangman_words, hangman_base_v2, '*a**a***'), number=100)) 

"""
Question 6

Replace the function input_user(pattern, guesses) by 
an automated version input_AI(pattern, guesses).

The AI version implements the strategy mentionned before:
 * Collect the set of words following the current pattern from 
   the base of words we are guessing from;
 * Calculate the probability of each letter 
   (the number of occurrences of a letter with respect to the number of occurrences of all letters) 
   that is not yet guessed based on the collected set of words;
 * Choose the letter with the highest probability. 
"""
def input_AI(pattern, guesses):
# REMOVE 'raise NotImplementedError', INSERT YOUR CODE HERE AND UNCOMMENT THE CALL
    raise NotImplementedError

#hangman_game(secret_word, input_AI)

"""
Question 7 (advanced only)

How many turns does the AI player need to be sure to win with the base of periodic elements?
"""
# in 7 turns, it wins for all words, in 6 turns it fails on 'cobalt'

