import pytest
import pangram

def test_isLoosePangram():
    assert pangram.isLoosePangram("azertyuiopqsdfghjklmwxcvbn") == True, "all letters of the azerty keyboard"
    assert pangram.isLoosePangram("azertyuiopqsdfghjklmwxcvbnwxcvbn") == True, "all letters of the azerty keyboard, last line twice"
    assert pangram.isLoosePangram("zertyuiopqsdfghjklmwxcvbn") == False, "missing the letter 'a'"
    assert pangram.isLoosePangram("AZERTYUIOPQSDFGHJKLMWXCVBN") == True, "case shouldn't matter"
    assert pangram.isLoosePangram("AZERTYUIOPqsdfghjklmWXCVBN") == True, "case shouldn't matter"
    assert pangram.isLoosePangram("azertyuiop qsdfghjklm wxcvbn") == True, "space characters should be ignored"

def test_isStrictPangram():
    assert pangram.isStrictPangram("aazertyuiopqsdfghjklmwxcvbn") == False, "the letter 'a' appears twice"
