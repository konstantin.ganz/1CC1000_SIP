def isLoosePangram(s):
    """Return True if the string contains 
    all the letters of the alphabet at least once
    (case is ignored)
    example: 'The quick and big brown fox jumps 
    over the lazy old dog' is a valid loose pangram.
    """
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    for char in alphabet.upper():
        if char not in s.upper():
            return False
    return True

def isStrictPangram(s):
    """Return True if the string contains 
    all the letters of the alphabet once and only once
    (case is ignored)
    example: 'The quick brown fox jumps over 
    the lazy dog' is a valid strict pangram.
    """
    if (not isLoosePangram(s)):
        return False
    if len(s) > 26:
        return False
    return True
