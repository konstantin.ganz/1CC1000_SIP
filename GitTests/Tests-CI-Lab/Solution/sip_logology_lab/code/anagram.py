def isAnagram(s1, s2):
  s1 = s1.replace(" ","")
  s2 = s2.replace(" ","")
  return (sorted(s1.upper())== sorted(s2.upper()))
