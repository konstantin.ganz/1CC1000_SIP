import string

def isPalindrome(word):
    word = word.replace(" ","")
    word = word.upper()
    word = word.translate(str.maketrans('','', string.punctuation))
    reversed_word = word[::-1]
    return word == reversed_word

