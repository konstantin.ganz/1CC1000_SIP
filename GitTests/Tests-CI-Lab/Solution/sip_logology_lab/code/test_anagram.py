import pytest
import anagram

def test_isAnagram():
    assert anagram.isAnagram("brainy", "binary") == True, "'brainy' and 'binary' both use exactly a, b, i, n, r, and y"
    assert anagram.isAnagram("lime", "time") == False, "'lime' uses 'l' that doesn't appear in 'time'"

def test_isAnagram_with_mixed_case():
    assert anagram.isAnagram("Geraldine", "realigned") == True, "anagrams are not case sensitive"

def test_isAnagram_with_space():
    assert anagram.isAnagram("The eyes", "They see") == True, "space characters should be ignored"

