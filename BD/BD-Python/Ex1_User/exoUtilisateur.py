# # PYTHON AND DATABASE
# 
# This file showcases how to interact with a database using Python:
# how to create tables, how to insert data or run other queries.
# 
# As a relational database management system (RDBMS) we use SQLite, 
# and we'll create and manipulate a SQLite database directly within 
# our Python application.

# In order to make a Python program communicate with a SQLite database,
# we need to have the Python package sqlite3 installed.
import sqlite3


# ## Presentation of the example
# 
# We develop an online authentication system. We'll create a database with 
# only one table:
# 
# ```
# User(
#     code (primary key, it can contain digits and letters!),
#     Name,
#     Password)
# ```

# ## DB Creation

# ```db_file``` will be the path to the file that will contain the database
db_file = "management.db"

# Open a connection to the database:
conn = sqlite3.connect(db_file)

# Execute the code above and notice how a file ```management.db``` is created 
# in your workspace.
# 
# Open it with sqlitebrowser: it contains no table (yet).    

# Create the ```cursor``` object used to execute queries to the database.
cursor = conn.cursor()

# ## Table creation
# 
# Read and complete the code of the ```create_table``` function below to create the table.
def create_table(conn, cursor):
    """
    Creates the database table

    Parameters
    ----------
    conn : 
        The object used to manage the database connection.
    cursor : 
        The object used to query the database.
    """

    print("Creating User table...")
    cursor.execute('''
        #
        #  write your SQL creation query here
        #
    ''')


# Execute the next cell and check the structure of your DB with sqlitebrowser.
"""
create_table(conn, cursor)
"""

# ## Data insertion
# 
# Read and complete the code of the ```insert_user``` function below.
"""
def insert_user(user, conn, cursor):
    """
"""
    Inserts a user into to the database.

    Parameters
    ----------
    user : dict
        A dictionary holding the user data: 
        user["code"], user["name"], user["password"].
    conn : 
        The object used to manage the database connection.
    cursor : 
        The object used to query the database.

    Returns
    -------
    bool
        True if no error occurs, False otherwise.
    """
"""
    insert_query = # A COMPLETER
    # hint:
    # INSERT INTO User (code, name, password) VALUES ("A1dep3", "Alice", "W0nder1@nd")
    print(insert_query)
    cursor.execute(insert_query)    
"""

# Execute the next cell and check that Alice and Bob were added to the database 
# with sqlitebrowser.
"""
alice = {"code": "A1dep3", "name": "Alice", "password": "W0nder1@nd"}
insert_user(alice, conn, cursor)
bob = {"code": "23TMP-1", "name": "Bob", "password": "Sp0ng!"}
insert_user(bob, conn, cursor)
"""

# ## Data extraction
# 
# Edit the following code so that the function returns True is the provided name 
# and password match a registered user.
"""
def isValid(name, password, conn, cursor):
    query = # A COMPLETER
    print(query)
    cursor.execute(query)
    data = cursor.fetchall()
    print(data)
    if # A COMPLETER
        return False
    else:
        return True
    
print("Alice: ", isValid("Alice", "W0nder1@nd", conn, cursor))
print("Bob: ",   isValid("Bob", "nonsense", conn, cursor))
"""

# ## SQL Injection
# 
# Imagine the name and password come from a web form. Find what an attacked may 
# enter into the web form to pass the authentication while he is not a registered user.
"""
print("Hacker",  isValid("A COMPLETER", "A COMPLETER", conn, cursor))
"""

# ## Parametrized query
# 
# A common way to guard against SQL injection is to use parameterized queries. 
# Such queries use placeholders in the SQL query (usually `?`). The actual values 
# are provided as separate arguments and Python database libraries automatically 
# escape any special characters in the values to prevent SQL injection. 
# 
# Note: the specific placeholder syntax used can vary depending on the programming 
# language, library, or database system being used. For instance you might also 
# find `%s`.
"""
def isValid_param(name, password, conn, cursor):
    query = "select name, password from User where User.name = ? and User.password = ?"
    cursor.execute(query, (name, password,))
    data = cursor.fetchall()
    print(data)
    if len(data) == 0:
        return False
    else:
        return True
    
print("Alice: ",  isValid_param("Alice", "W0nder1@nd", conn, cursor))
print("Bob: ",    isValid_param("Bob", "nonsense", conn, cursor))
print("Hacker:",  isValid_param("Charly", "blablabla' or '1'='1", conn, cursor))
"""

# ## Close the connection to the database
cursor.close()
conn.close()