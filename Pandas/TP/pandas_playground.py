"""
This code showcases the main functionalities of the Pandas library.
"""
import pandas as pd
from pandas.core.frame import DataFrame

from utilities import get_date_as_ddmmyyyy

#
# Entry point of this module
#
if __name__ == "__main__" :

    ################### STEP 1: read a CSV file into a Pandas dataframe ###################

    # TODO: create a DataFrame named "input_df" to hold the content of 
    # the CSV file "books.csv" that you'll find in the data directory.
    # hint: use the 'read_csv' function of the Pandas library
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html
    # - from the PandasLab directory, you must go into data to find books.csv
    # - is the books.csv file using the separator Pandas expects by default?
    # insert your code here and uncomment the following print statement
    #print(input_df)
         
    # TODO: display the header of the columns (first line of the CSV file)
    # hint: use 'columns'
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.columns.html
    #print("names of the columns:")
    # insert your code here and uncomment the previous print statement
    # expected result:
    """
    Index(['bookID', 'title', 'authors', 'average_rating', 'language_code', '  num_pages', 'publication_date', 'publisher'], dtype='object')
    """
    
    # or even better (optional): 
    """
    ['bookID', 'title', 'authors', 'average_rating', 'language_code', '  num_pages', 'publication_date', 'publisher']
    """
    
    # TODO: display the 3 first records (that 1 line of header, and 3 lines of values)
    # hint: use 'head'
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.head.html
    #print("3 first records:")
    # insert your code here and uncomment the previous print statement
    # expectd result:
    """
       bookID                                              title                     authors  average_rating language_code    num_pages publication_date        publisher
0       1  Harry Potter and the Half-Blood Prince (Harry ...  J.K. Rowling/Mary GrandPré            4.57           eng          652        9/16/2006  Scholastic Inc.
1       2  Harry Potter and the Order of the Phoenix (Har...  J.K. Rowling/Mary GrandPré            4.49           eng          870       09/01/2004  Scholastic Inc.
2       4  Harry Potter and the Chamber of Secrets (Harry...                J.K. Rowling            4.42           eng          352       11/01/2003       Scholastic
    """
    # Notice that the index is present in the output (the first column of the CSV file is in fact displayed
    # at the second position in the output, the value in first position is the index)
    

    ################### STEP 2: selection ###################

    # TODO: create a new dataframe called "books" that only contains the data in the columns
    # bookID, title, authors, language_code, and publication_date.
    # hint: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#basics
    # insert your code here and uncomment the following print statement
    #print(books)
   
    # TODO: print the last row of the books dataframe
    # hint: 'tail'
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.tail.html
    #print("last row of the 'books' DataFrame:")
    # insert your code here and uncomment the previous print statement
    # expected result:
    """
           bookID                        title     authors language_code publication_date
11122   45641  Las aventuras de Tom Sawyer  Mark Twain           spa        5/28/2006
    """


    ################### STEP 3: find and replace ###################

    # TODO: print the rows 4 to 6 of 'books'
    #print("rows 4, 5 et 6 of the 'books' before replacement:")
    # insert your code here and uncomment the previous print statement
    # expected result:
    """
       bookID                                              title                     authors language_code publication_date
4       8  Harry Potter Boxed Set  Books 1-5 (Harry Potte...  J.K. Rowling/Mary GrandPré           eng        9/13/2004
5       9  Unauthorized Harry Potter Book Seven News: "Ha...      W. Frederick Zimmerman         en-US        4/26/2005
6      10       Harry Potter Collection (Harry Potter  #1-6)                J.K. Rowling           eng       09/12/2005
    """

    # Notice that in language_code column, the code for "English" is either "eng" or "en-US".
    # TODO: Replace both values with the code "en".
    # hint: use the 'replace' function of DataFrame
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.replace.html
    # - focus on the documentation of the 'to_replace' parameter of dict type (and more specifically "nested dictionaries")
    # - assign the result back to 'books': books = books.replace(....)
    #insert your code here

    # TODO: print the rows 4 to 6 again.
    #print("rows 4, 5 et 6 of the 'books' after replacement:")
    # insert your code here and uncomment the previous print statement
    # expected result:
    """
       bookID                                              title                     authors language_code publication_date
4       8  Harry Potter Boxed Set  Books 1-5 (Harry Potte...  J.K. Rowling/Mary GrandPré            en        9/13/2004
5       9  Unauthorized Harry Potter Book Seven News: "Ha...      W. Frederick Zimmerman            en        4/26/2005
6      10       Harry Potter Collection (Harry Potter  #1-6)                J.K. Rowling            en       09/12/2005
    """
    
    ################### STEP 4: removing duplicates ###################

    # TODO: create a new dataframe called "publishers" that only contains 
    # the data in the column "publisher" of the original dataframe input_df.
    # insert your code here and uncomment the following print statement

    # Since a publisher may have published several books, its name appears 
    # multiple times in this dataframe.
    # TODO: uncomment the following line to see that publisher names are indeed repeated.
    #print(publishers)
    # TODO: print the number of rows of "publishers"
    # hint: 
    # - you can look at the length of the index 
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.index.html
    # - or you can look at the 'shape' property that gives the dimensions of the DataFrame 
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.shape.html
    #print("number of rows in 'publishers' with the duplicates:")
    # insert your code here and uncomment the previous print statement
    # expected result: 11123

    # TODO: eliminate redundant data
    # hint: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.drop_duplicates.html
    # insert your code here

    # TODO: print the new number of rows of "publishers"
    #print("number of rows in 'publishers' without the duplicates:")
    # insert your code here and uncomment the previous print statement
    # expected result: 2290


    ################### STEP 5: working with dates ###################
    
    # TODO: display the publication date of the item of the first row
    # hint: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.at.html
    #print("publication date of first item before transformation:")
    # insert your code here and uncomment the previous print statement
    # expected result: 9/16/2006

    # Looking at the input file, we notice that all the dates in the publication_date column 
    # are in fact in the format mm/dd/yyyy;
    # we want to convert them into the format dd/mm/yyyy.
    # TODO: Iterate over all values in column publication_date and apply the conversion function 
    # get_date_as_ddmmyyyy() that is defined in the utilities.py file.
    # hint: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.map.html
    # - 'map' applies to a Series, not a DataFrame, a given column is a Series
    # - don't forget to reassign back to the DataFrame column the new Series!
    # insert your code here

    # TODO: display the publication date of the item of the first row again
    #print("publication date of first item after transformation:")
    # insert your code here and uncomment the previous print statement
    # expected result: 16/09/2006
