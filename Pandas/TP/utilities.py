import pandas as pd
# The Python module datetime defines the necessary types for manipulating 
# dates and times.
# Among these types, we find one called datetime (the same name as the module) 
# that is used to express a combination of date and time. 
from datetime import datetime

def get_date_as_ddmmyyyy(input_date):
    """
    Gets a date as a string in the format mm/dd/yyyy and 
    returns that date as a string in the format "dd/mm/yyyy".

    Parameters
    ----------
        input_date: string
            The input date (in the format mm/dd/yyyy)

    Returns
    -------
        A string
            The input date in the format dd/mm/yyyy, or None if the input date is null.
    """
    
    # In case we have an empty string we return None
    if pd.isnull(input_date):
        return None

    # In order to convert the input date into the right format, we can use 
    # the strftime() function defined in the datetime Python module.
    #
    # Unfortunately, this function cannot directly convert a string to another string;
    # but it can convert a variable of type "datetime" to a string.
    # Therefore, we must first convert the input_date from a string to a datetime object.
    #
    # In order to turn the input_date into a datetime object, we can use
    # the strptime() function of the datetime type; it takes in two arguments:
    # - First argument: a string containing a date.
    # - Second argumnet: a string describing the format of the date given in the first argument.
    # The format must be expressed according to a well-defined code.
    # For instance:
    # * %m indicates a month as a zero-padded decimal number (01, 02, ....);
    # * %y indicates a year without century as a zero-padded decimal number (07, 20, ...99);
    # * %Y indicates a year as a decimal number (2019, 2020)...
    # The complete list of the format codes can be found at the following address:
    # https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior
    # In the code below, the format "%m/%d/%Y" indicates that the date 
    # given in the first argument must be mm/dd/yyyy. 
    # If the date is not in this format, or if the date is not valid (11/31/2000 for instance),
    # a ValueError exception is raised. 
    try:
        input_date = datetime.strptime(input_date, "%m/%d/%Y")
    except ValueError:
        return None
    
    # Now input_date is an object of type datetime.
    # We convert the date to a string, enforcing the format that we want,
    # that is dd/mm/yyyy.
    return input_date.strftime("%d/%m/%Y")

