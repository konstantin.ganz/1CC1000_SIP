# How to use NumPy
 
# The objective of these examples is to see how an effective use of NumPy 
# makes it possible to carry out efficient calculations.

## When is NumPy effective?"

### List of lists
 
# The following code:
# - creates a list of lists, `A`, randomly generated, containing a large number 
#   of 0 and 1;
# - makes a second list of lists, the same size as `A` and which we subtly call `B`;
# - copies the values from `A` to `B`. 
# - measures the execution time of the *copy* opeation.
# You should have a time of the order of a few hundredths of seconds. Test it.
from random import randint
from timeit import timeit
 
A = [[randint(0,1) for _ in range(300)] for _ in range(600)]
B = [[0 for _ in range(300)] for _ in range(600)]
 
def copy(A, B):
    for i in range(0, len(A)):
        for j in range(0, len(A[0])):
            B[i][j] = A[i][j]
 
timeit("copy(A, B)", number=10, globals=globals())

### NumPy array
 
# The following code is used to perform an equivalent operation using NumPy.
 
# Compare its run time. Does this surprise you?
"""
import numpy as np
 
A = np.random.randint(0, 2, (300, 600))
B = np.zeros(A.shape)
 
def copy(A, B):
    for i in range(0, len(A)):
        for j in range(0, len(A[0])):
            B[i,j] = A[i,j]
 
timeit("copy(A, B)", number=10, globals=globals())
"""
### The "good way"
 
# It seems that using NumPy is less effective than using lists. The reality is 
# that we are not using NumPy functionality properly. Indeed, this library is 
# optimized for "vectorization" or "array programming". It is a technique of 
# applying operations to one set of data at a time.
 
# To take advantage of NumPy, you must therefore change your approach and use 
# the features offered by this library. For example, the correct way to perform 
# a copy operation is the following. You should agree that it is significantly 
# more effective... 
"""
A = np.random.randint(0, 2, (300, 600))
B = np.zeros(A.shape)
 
def copy(A, B):
    A[:,:] = B[:,:]
 
timeit("copy(A, B)", number=10, globals=globals())
"""
### The `vectorize` operation
 
# In NumPy, every mathematical operation with arrays is automatically vectorized.
# If you wrote your own function and want to see it applied to every elements of 
# an array without using loops, use the `vectorize()` operation :
"""
def my_function(x):
    if x < 0:
        return 0
    else:
        if x > 1000000:
            return x - 1000000
        else:
            return x*x
 
my_vectorized_function = np.vectorize(my_function)
 
def v1(A):
    for i in range(0, len(A)):
        for j in range(0, len(A[0])):
            A[i,j] = my_function(A[i,j])

Da = np.random.randint(2, 5, (2, 3))
Db = np.zeros(Da.shape, dtype=int)
Db[:,:] = Da[:,:]
print("Da:", Da)
print("Db:", Db)

print("check the results:") 
print("without vectorization:")
v1(Da)
print(Da)
 
print("with vectorization:")
Db = my_vectorized_function(Db)
print(Db)
 
Da = np.random.randint(-2, 3, (1000, 1000))
Db = np.zeros(Da.shape, dtype=int)
Db[:,:] = Da[:,:]
print("without vectorization: ", timeit("v1(Da)", number=10, globals=globals()))
print("with vectorization:    ", timeit("my_vectorized_function(Db)", number=10, globals=globals()))
"""
## How to use NumPy"

### Slice of lists
 
# You already know the slice of lists. Here is an example, we note that :
# - when used to the left of an equal sign, it is used to set values in a list;
# - when used to the right of an equal sign, it is used to duplicate part of a list.
"""
l = [0, 1, 2, 3, 4, 5]
print("Id of l                     : ", id(l))
print("initial content of l        :  ", l)
""" 
# slice on the left of =  -> set values
"""
print("change the 3 first values stored in l")
l[:3] = [2, 1, 0]
print("Id of l is unchanged        : ", id(l))
print("new content of l            :  ", l)
""" 
# slice on the rigth of = -> duplicate part of a list
"""
print("l2 is a copy of a sub-part of l")
l2 = l[:4]
print("Id of l2 differs from id of l: ", id(l2))
print("content of l2                : ", l2)
 
print("change the values stored in l2")
l2[:] = [0, 0, 0, 0] # Warning do not confuse with "l2 = [0, 0, 0, 0]" 
                     # which makes l2 points towards a new reference
print("new content of l2           :  ", l2)
print("content of l is unchanged   :  ", l)
"""
### Views
 
# Using the bracket notation on a NumPy array does not create a slice, 
# it works differently from the lists. The bracket notation to the left or 
# to the right of an equal sign will always create a view, which is an access 
# to the data but is never a copy of the data.
 
# Test the following code.
"""
A = np.array([0, 1, 2, 3, 4, 5])
print("Id of A                     : ", id(A))
print("initial content of A        :  ", A)
""" 
# bracket notation on the left of =
"""
print("change the 3 first values stored in A")
A[:3] = [2, 1, 0]
print("Id of A is unchanged        : ", id(A))
print("new content of A            :  ", A)
""" 
# bracket notation of the right of =
"""
print("A2 is a view on part of A")
A2 = A[:4]
print("Id of A2 differs from id of A: ", id(A2))
print("content of A2                : ", A2)
 
print("change the values stored in A2")
A2[:] = [0, 0, 0, 0]
print("new content of A2                 :  ", A2)
print("content of A is changed as well!  :  ", A)
"""
# Array or view can most of the time be used without distinction. When 
# handling a view, the base attribute allows us to find the corresponding array.
"""
print("Base of A  :  ", A.base)
print("Base of A2 : ", A2.base)
 
print("Id of A              : ", id(A))
print("Id of the base of A2 : ", id(A2.base))
"""
# A view is a link to data and an organization of data. With reshape we can 
# create new views towards data which can be organized differently.
#A = np.array([0, 1, 2, 3, 4, 5])
"""
V3 = np.reshape(A, (2, 3))
print("V3 from reshape of A  :\\n", V3)
V3[1, 0] = 9
print("V3 after value update :\\n", V3)
print("A after V3 is modified:\\n", A)
"""
### Indexing
 
# It is possible to reference indices in ways quite similar to the slice 
# (start, end, step), however when handling NumPy arrays with several dimensions, 
# it is not necessary to chain the bracket notations (to access lists of lists 
# of ...), just use commas.
 
# Finally, we can fix the values contained in a view using another view of the 
# same size and same dimension or a scalar.
 
# For more informations : [indexing ](https://numpy.org/doc/stable/reference/arrays.indexing.html)"
"""
A = np.ones((3, 4))
print("Array A :", A)
 
A[::2, :] = 2 # lignes 0 et 2, toutes les colonnes, remplir de 2
print("Array A : modifié avec un scalaire", A)
 
B = np.zeros((3, 4))
A[:,:] = B[:,:]
print("Array A : modifié avec une vue", A)
"""