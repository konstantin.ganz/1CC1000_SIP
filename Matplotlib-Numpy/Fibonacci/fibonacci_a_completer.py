# Introduction à Matplotlib et Numpy

# L'objectif de cet exercice est de se familiariser avec 
# la bibliothèque Numpy destinée aux calculs scientifiques et 
# la bibliothèque Matplotlib pour la visualisation des résultats sous forme de courbes.
# C'est également l'occasion de découvrir comment mesurer le temps d'exécution 
# d'une fonction.
# Pour cela, vous allez développer plusieurs versions d'une fonction 
# calculant les termes de la suite de Fibonacci, mesurer les performances et 
# tracer des graphiques comparatifs.

##===============================================##
## Temps d'exécution de deux versions classiques ##
##===============================================##

### Version 1 : fonction récursive
#=================================
# Développez une fonction récursive `fib_rec(n)` calculant le n ième terme de 
# la suite de Fibonacci.
def fib_rec(n):
    pass # A REMPLACER PAR VOTRE CODE

# Vérifiez que votre fonction produit les bons résultats 
# en exécutant les tests suivants :
"""
assert fib_rec(0) == 0
assert fib_rec(1) == 1
assert fib_rec(2) == 1
assert fib_rec(3) == 2
assert fib_rec(10) == 55
"""
### Mesure du temps d'exécution
#==============================
# La fonction `timeit` du package éponyme permet de mesurer le temps d'exécution 
# d'un code Python. 
# Dans l'exemple suivant, `timeit` renvoie le temps total de 100 exécutions 
# du code Python pour lisser les résultats. 
# Ces résultats peuvent varier d'une machine à l'autre, mais vous devez 
# vous attendre à un temps d'exécution de quelques millièmes de seconde. 
# Testez-le.
from timeit import timeit
"""
print(timeit(lambda : fib_rec(10), number=100))
"""
# Nous mettons à votre disposition la fonction `mesureTpsExec` qui vous permet 
# de déterminer le temps d'exécution d'une fonction pour une séquence de valeurs 
# de paramètres. 
# Au-delà de n=20, vous devriez observer des temps significatifs.
def mesureTpsExec(func, values, number=100):
    m = []
    for i in values :
        m.append(timeit(lambda : func(i), number=number))
    return m
"""
print(mesureTpsExec(fib_rec, [1, 2, 3, 5, 10, 20, 25]))
"""
### Version 2 : fonction itérative
#=================================
# Développez une fonction itérative `fib_iter(n)` calculant la valeur 
# du n ième terme de la suite de Fibonacci.
def fib_iter(n):
    pass # à remplacer par votre code
# Ecrivez quelques tests pour vérifier que les résultats produits sont corrects.
"""
assert fib_iter(0) == 0           # à compléter
"""
# Mesurez son temps d'exécution. 
# Cette version devrait être sensiblement plus rapide que la version récursive.
"""
print('version itérative: ', A COMPLETER)
"""
##================================================##
## Représentation graphique des temps d'exécution ##
##================================================##
# Nous allons à présent représenter les temps d'exécution en fonction de `n` 
# pour les 2 versions de la fonction sur un graphe.
# Nous utilisons le module `pyplot` de la bibliothèque `matplotlib` :
import matplotlib.pyplot as plt
# La fonction `plot` permet d'afficher une série de points définie par 2 listes, 
# celle des abscisses et celle des ordonnées :
##     plt.plot(xvalues, yvalues, label="légende de la courbe")
# D'autres méthodes permettent de donner un titre au graphique, aux axes, 
# de demander un axe des ordonnées suivant une échelle logarithmique, 
# de choisir le style des lignes (":", "-", "-." et "--", particulièrement utile 
# pour les daltoniens ou lorsqu'on imprime en noir et blanc)...
##     plt.title("Name")
##     plt.xlabel("Name")
##     plt.ylabel("Name")
##     plt.yscale("log")
##     plt.plot(xvalues, yvalues, "--", label="Dashed line")
##     plt.legend()
# Pour terminer, il faut demander l'affichage :
##     plt.show()

# Ecrivez une fonction `iter_vs_rec` qui mesure les temps d'exécution 
# de `fib_rec` et `fib_iter` pour quelques valeurs de `n` 
# et les affiche sur un graphe.
"""
import A COMPLETER

def iter_vs_rec():
    listeValeurs_n = list(range(1, 20, 1))
    # tpsExecRec = A COMPLETER
    # tpsExecIter = A COMPLETER

    # plt.title("Comparaison du temps d'exécution de 2 versions de la fonction de Fibonacci")
    # A COMPLETER
    # plt.show()
iter_vs_rec()
"""
##=======================================##
## Version 3 : en utilisant des matrices ##
##=======================================##
# Il existe (au moins) une autre manière de calculer les éléments 
# de la suite de Fibonacci, en utilisant :
# ( Fn   )   ( 0 1 )^n   ( F0 )
# (      ) = (     )   * (    )
# ( Fn+1 )   ( 1 1 )     ( F1 )

### produit matriciel avec `numpy`
#=================================
# Pour faire le produit matriciel, nous allons utiliser `numpy`.
# Quelques éléments (de révision sans doute) :
# création d'une matrice et d'un vecteur :
##     import numpy as np
##     mat1 = np.array([[5,6],[10,3]])
##     vec = np.array([[9],[2]])
# accès à un élément de la matrice (2ème ligne, 1ère colonne) :
##     mat1[1,0]     # => 10
# quelques opérations :
##     mat2 = mat1 + mat1  # => mat2[i,j] = mat1[i, j] + mat1[i,j]
##     mat3 = mat1 * 2     # => mat3[i,j] = mat1[i,j] * 2
##     mat4 = np.matmul(mat1, mat1)      # matrix product
##     mat5 = mat1 @ mat1            # abbreviated product syntax
##     mat6 = np.linalg.matrix_power(mat1, 2)   # mat1^2

# **Attention !** 
# Vous avez peut-être l'habitude d'utiliser `np.matrix` plutôt que `np.array`. 
# Mais `np.matrix` va bientôt disparaitre de la bibliothèque, 
# il faut arrêter de l'utiliser et lui préférer `np.array`.

### Version 3
#============
# Ecrivez une fonction `fib_mat(n)` utilisant des `np.array` pour calculer 
# le n ieme terme de la suite de Fibonacci.
import numpy as np

def fib_mat(n):
    pass # à remplacer par votre code

# Ecrivez quelques tests pour vérifier que les résultats produits sont corrects.
"""
assert fib_mat(0) == 0  # A COMPLETER
"""
### Le diable est dans les détails
#=================================
# Comparez les résultats de `fib_iter(92)` et `fib_mat(92)` 
# puis de `fib_iter(93)` et `fib_mat(93)`
"""
print('fib_iter(92) = ', fib_iter(92))
# A COMPLETER
"""
# Explication : 
# Python ne limite pas les valeurs entières : quand l'interpréteur détecte un 
# dépassement de capacité (*overflow*), un nombre plus grand de bits est utilisé 
# pour coder la valeur. 
# Ce n'est pas le cas de `numpy` : les entiers sont signés et codés sur 64 bits 
# (de -9223372036854775808 à 9223372036854775807), et les dépassements de capacité 
# non détectés. 
# Conclusion : `numpy` est une bibliothèque pratique 
# mais à utiliser avec précaution !"

# Ajoutez la fonction `fib_mat` à votre graphe de comparaison des temps d'exécution.
def comparaisonTpsExec():
    pass # A COMPLETER
comparaisonTpsExec()

### grandes valeurs de n
#=======================
# Le temps d'exécution de la version récursive croît de manière exponentielle avec `n`.
# Mesurez les temps d'exécution pour trouver la valeur de `n` pour laquelle 
# l'exécution sur votre machine prend plus de 10 secondes.
# A FAIRE

# Adaptez le code suivant pour permettre de tracer le plus de valeurs possibles 
# dans un temps raisonnable.
def comparaisonTpsExec():
    listeValeurs_n_petites = list(range(1, 24, 6))
    listeValeurs_n_moyennes = list(range(1, 20, 1)) + list(range(20, 92, 4))
    listeValeurs_n_grandes = list(range(1, 20, 1)) + list(range(20, 92, 4)) + list(range(100, 500, 10))
    tpsExecRec = mesureTpsExec(fib_rec, listeValeurs_n_petites)
    tpsExecIter = mesureTpsExec(fib_iter, listeValeurs_n_grandes)
    tpsExecMat = mesureTpsExec(fib_mat, listeValeurs_n_moyennes)

    plt.title("Comparaison du temps d'exécution de 3 versions de la fonction de Fibonacci")
    plt.xlabel("n")
    plt.xticks(list(range(0, 2000, 100)))
    plt.ylabel("Temps d'exécution (en secondes) de fib(n)")
    plt.yscale("log")
    plt.plot(listeValeurs_n_petites, tpsExecRec, "-.", label="version récursive")
    plt.plot(listeValeurs_n_grandes, tpsExecIter, "--", label="version itérative")
    plt.plot(listeValeurs_n_moyennes, tpsExecMat, ".", label="version avec les matrices numpy")
    plt.legend()
    plt.show()
"""
comparaisonTpsExec()
"""
##=============================================================##
## Version 4 : avec mémoïsation (cette partie est optionnelle) ##
##=============================================================##
# Les mauvaises performances de la fonction récursive viennent du nombre d'appels 
# récursifs exponentiellement croissant. Il est possible de contourner ce problème 
# en utilisation la mémoïsation. Le principe est de stocker les résultats 
# intermédaires dans un dictionnaire et de vérifier, avant l'exécution d'un 
# appel récursif, si le résultat de cet appel est déjà connu dans le dictionnaire.

# Modifiez l'implémentation de votre fonction récursive pour exploiter la 
# mémoïsation et comparez son temps d'exécution aux autres fonctions.

# Si vous êtes en difficulté, la version française de la page Wikipedia présentant 
# la mémoïsation (https://fr.wikipedia.org/wiki/M%C3%A9mo%C3%AFsation) utilise 
# justement la suite de Fiabonacci comme exemple...
"""
def fib_rec_vMemo1(n, memo={}):
    if n == 0 or n == 1:
        return n
    else:
        # A COMPLETER

assert fib_rec_vMemo1(0) == 0
assert fib_rec_vMemo1(1) == 1
assert fib_rec_vMemo1(2) == 1
assert fib_rec_vMemo1(3) == 2
assert fib_rec_vMemo1(10) == 55
"""
# En Python, lorsqu'on donne à un paramètre une valeur par défaut qui un objet 
# mutable (dictionnaire, liste...), cet objet est conservé entre les appels. 
# Par conséquent, dans le code précédent le dictionnaire de mémoïsation n'est 
# complété qu'une unique fois. Cela fausse les comparaisons de temps d'exécution 
# car le module `timeit` fait une moyenne sur plusieurs appels.
#
# Deux solutions sont possibles : soit utiliser une fonction auxiliaire pour 
# explicitement initialiser le dictionnaire, soit fixer la valeur par défaut à `None`.
"""
def fib_rec_vMemo2(n, memo=None):
    if n == 0 or n == 1:
        return n
    else:
        # A COMPLETER

assert fib_rec_vMemo2(0) == 0
assert fib_rec_vMemo2(1) == 1
assert fib_rec_vMemo2(2) == 1
assert fib_rec_vMemo2(3) == 2
assert fib_rec_vMemo2(10) == 55
assert fib_rec_vMemo2(500) == fib_iter_v1(500)

def comparaisonTpsExec():
    listeValeurs_n_petites = list(range(1, 24, 6))
    listeValeurs_n_moyennes = list(range(1, 20, 1)) + list(range(20, 92, 4))
    listeValeurs_n_grandes = list(range(1, 20, 1)) + list(range(20, 92, 4)) + list(range(100, 500, 10))
    tpsExecRec = mesureTpsExec(fib_rec, listeValeurs_n_petites)
    tpsExecIter = mesureTpsExec(fib_iter, listeValeurs_n_grandes)
    tpsExecMat = mesureTpsExec(fib_mat, listeValeurs_n_moyennes)
    tpsExecRecMemo = mesureTpsExec(fib_rec_vMemo2, listeValeurs_n_grandes)

    plt.title("Comparaison du temps d'exécution de 3 versions de la fonction de Fibonacci")
    plt.xlabel("n")
    plt.xticks(list(range(0, 2000, 100)))
    plt.ylabel("Temps d'exécution (en secondes) de fib(n)")
    plt.yscale("log")
    plt.plot(listeValeurs_n_petites, tpsExecRec, "-.", label="version récursive sans mémoïsation")
    plt.plot(listeValeurs_n_grandes, tpsExecRecMemo, ":", label="version récursive avec mémoïsation")
    plt.plot(listeValeurs_n_grandes, tpsExecIter, "--", label="version itérative")
    plt.plot(listeValeurs_n_moyennes, tpsExecMat, ".", label="version avec les matrices numpy")
    plt.legend()
    plt.show()
comparaisonTpsExec()
"""