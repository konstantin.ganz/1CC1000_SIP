import tkinter as tk

# Event handling
#---------------
# The interfaces that we played with so far do not react to the user's actions. Each action (e.g., typing text in a text field, clicking a button) generates an **event**. For each event that we want to capture, we have to write the code that will be executed in response to that event; in other words, we associate to each event a function that is called when the event happens. This function is called an **event handler** or **callback**.

window = tk.Tk()

def pressed():
    print("I was pressed!")
    
tk.Button(window, text="press me!", command=pressed).pack()

window.mainloop()

# widgets with a value
#---------------------
# Widgets that allow the user to input a value have an argument ```variable``` or equivalent. This argument is passed a *tk variable*. The type of the variable depends on the widget; for instance for an ```Entry``` the argument is called ```textvariable``` and expects a ```tk.StringVar```. When the user types something in or clicks something, this variable gets assigned a value which can be exploited elsewhere, in the callback function of a button for instance:
"""
window = tk.Tk()

inputName = tk.StringVar()
def pressed():
    print("Name when I was pressed: " + inputName.get())

tk.Entry(window, textvariable=inputName).pack()
tk.Button(window, text="OK", command=pressed).pack()

window.mainloop()
"""
# In the case of a ```Scale``` widget, the argument is named ```variable``` and expects either a ```IntVar```, a ```DoubleVar```, or a ```StringVar``` (in the latter case, the numerical value will be converted to a string):
"""
window = tk.Tk()

score = tk.IntVar()
def pressed():
    print("Validated score: " + str(score.get()))

tk.Scale(window, orient='horizontal', from_=5, to=10, variable=score).pack()
tk.Button(window, text="OK", command=pressed).pack()

window.mainloop()
"""
# Yet not all widgets set an associated *variable*. The selected item(s) in a ```Listbox``` for instance are retrieved by calling the ```curselection``` function on the list:
"""
window = tk.Tk()

fruitsListBox = tk.Listbox(window)
fruitsListBox.insert(tk.END,"apple")
fruitsListBox.insert(tk.END, "banana")
fruitsListBox.insert(tk.END, "cherry")
fruitsListBox.pack()

def pressed():
    print(fruitsListBox.get(fruitsListBox.curselection()))

tk.Button(window, text="OK", command=pressed).pack()

window.mainloop()
"""
# ```bind()```
#-------------
# If no callback function was associated with a widget at its creation, it's possible to add one later using the ```bind``` function. ```unbind``` detaches the callback function from the widget.

# The ```bind``` function takes 2 arguments. The first argument indicates the event that triggers the callback, such as ```<Button-1>``` for a click with the left button of the mouse, ```<Leave>``` when the mouse point exits the perimeter of the widget, ```<Return>``` if the user pressed the Enter key... The second argument is the name of the callback function.
"""
window = tk.Tk()

def pressed(event):
    print("I was pressed once, I don't want to listen to clicks anymore.")
    OKbutton.unbind('<Button>')

def entered(event):
    print("The mouse pointer was over me")

OKbutton = tk.Button(window, text="OK")
OKbutton.pack()

OKbutton.bind("<Button>", pressed)
OKbutton.bind("<Enter>", entered)

window.mainloop()
"""
# Notice that the callback functions expect a *event* parameter. It's possible to bind different events to the same callback function. The *event* parameter then indicates which event indeed triggered the function. The parameter also carries additionnal information about the event, for instance for a mouse click it gives to position of the mouse pointer.

# For a list of common events name and type see https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/event-types.html
"""
window = tk.Tk()

def somethingCameUp(event):
    print(event)
    if event.type == "4":
        print("I was pressed at x=%d, y=%d" %(event.x, event.y))
    elif event.type == "7":
        print("The mouse pointer was over me")
    
OKbutton = tk.Button(window, text="OK")
OKbutton.pack()

OKbutton.bind("<Button>", somethingCameUp)
OKbutton.bind("<Enter>", somethingCameUp)

window.mainloop()
"""