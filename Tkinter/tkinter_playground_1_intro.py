# The Tkinter library
#====================
# The Graphical User Interface (GUI) allows users to access all the application functionalities in an easy and intuitive way.
# Several libraries, known as *widget toolkits*, exist to program a GUI in Python. For this tutorial, we choose **Tkinter**, which is built on top of the Tcl/Tk widget toolkit, for two reasons:
# - It is the standard built-in Python GUI library.
# - As opposed to other libraries, it is easy to learn.
# As a downside, the interfaces programmed with Tkinter look a bit rudimentary. However, if we need to quickly develop simple interfaces, Tkinter is an excellent choice.

import tkinter as tk # module name all in lowercase (was \"Tkinder\" with python2)

# The main window
#================
# The main element of a GUI is the **window**, that is a container that includes all the other GUI elements, usually referred to as *widgets*.
# A windows is created by a call to the ```Tk()``` function.
# Once created, you can set many properties of the windows such as its title, and add widgets to it.
# Eventually, you call its ```mainloop()``` function that triggers the execution of the so-called *event loop*. During the event loop, the GUI listens for any event that might be triggered by the users' actions on the widgets, such as buttons clicks, keypresses and so on. Importantly, calling the function window.mainloop() blocks any code that comes after it. Closing the window breaks the event loop.

# Execute the next snippet. You should read the message *Opening the window* and see an empty window entitled \"Playground\", while the message *The window is now closed* does not appear and the script keeps running. Close the Playground window (by clicking on the upper right X sign) to exit the event loop, execute the last print statement and complete the script.
"""
window = tk.Tk()                   # create a window
window.title("Playground")         # set the titlebar
print("Opening the window")
window.mainloop()                  # listen for interaction events
print("The window is now closed")
"""
# A window with a widget
#=======================
# Tkinter offer many different widgets such as labels (to display text), text fields (fields where the user can type text in), buttons, radio buttons, check buttons, option menus, scale...
# When you create a widget, you always specify the parent window it is attached to, and properties that are specific to that kind of widget.
# Next you call the function ```pack()``` on the widget to invoke the *geometry manager*, responsible for placing the widgets in a window. First, the window is created with a certain size. Each time we invoke the function ```pack``` on a widget of that window, the geometry manager places the widget in the topmost portion of the window that is not occupied by another widget. When all the widgets are placed, the window is made large enough to hold them. We'll see later how to get a different arrangement.

# Execute the following code snippet and observe how the window is resized to match the content of the label (but you can still resize it using the mouse).
"""
windowWithLabel = tk.Tk()                   # re-create the window (was destroyed when closed to end the previous code snippet)\n",
windowWithLabel.title("Playground")
helloLabel = tk.Label(windowWithLabel, text="Hello World!")
helloLabel.pack()
windowWithLabel.mainloop()
"""